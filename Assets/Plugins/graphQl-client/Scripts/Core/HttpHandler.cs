﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
//using System.Net.WebSockets;
using NativeWebSocket;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace GraphQlClient.Core
{
    public class GraphQlSubscriptionWebSocket : WebSocket
    {
        public Queue<string> messageQueue = new Queue<string>();

        public event Action OnSubscriptionHandshakeComplete;
        public event Action<string> OnSubscriptionDataReceived;
        public event Action OnSubscriptionCanceled;

//        public new event WebSocketOpenEventHandler OnOpen;
//        public new event WebSocketMessageEventHandler OnMessage;
//        public new event WebSocketErrorEventHandler OnError;
//        public new event WebSocketCloseEventHandler OnClose;

        public GraphQlSubscriptionWebSocket(string url, Dictionary<string, string> headers = null) : base(url.Replace("http", "ws"), headers)
        {
        }

        public GraphQlSubscriptionWebSocket(string url, string subprotocol, Dictionary<string, string> headers = null) : base(url.Replace("http", "ws"), subprotocol, headers)
        {
        }

        public GraphQlSubscriptionWebSocket(string url, List<string> subprotocols, Dictionary<string, string> headers = null) : base(url.Replace("http", "ws"), subprotocols, headers)
        {
        }


        public GraphQlSubscriptionWebSocket(string url, string protocol = "graphql-ws", string authToken = null) : base(url.Replace("http", "ws"), new List<string>() { protocol }, (!string.IsNullOrEmpty(authToken) ? new Dictionary<string, string>() { { "Authorization", "Bearer " + authToken } } : null))
        {
        }

        public GraphQlSubscriptionWebSocket(string url, string protocol = "graphql-ws") : base(url.Replace("http", "ws"), new List<string>() { protocol }, null)
        {
        }

        public GraphQlSubscriptionWebSocket(string url) : base(url.Replace("http", "ws"), new List<string>() { "graphql-ws" }, null)
        {
        }

        public async Task Subscribe(GraphApi.Query query, string socketId = "1")
        {
            if (String.IsNullOrEmpty(query.query))
                query.CompleteQuery();

            try
            {
                Init();
                SendQuery(socketId, query.query);

                OnMessage += OnMessageReceived;
                await this.Connect();
            }
            catch (Exception e)
            {
                Debug.Log("woe " + e.Message);
            }
        }

        private void OnMessageReceived(byte[] data)
        {
            try
            {
                string message = Encoding.UTF8.GetString(data ?? throw new ApplicationException("data = null"));
                if (OnSubscriptionDataReceived == null)  // Don't handle data if event-handler not set
                    return;

                if (string.IsNullOrEmpty(message))
                    return;
                JObject obj;
                try
                {
                    obj = JObject.Parse(message);
                }
                catch (JsonReaderException e)
                {
                    throw new ApplicationException(e.Message);
                }

                string subType = (string)obj["type"];
                switch (subType)
                {
                    case "connection_ack":
                    {
                        Debug.Log("init_success, the handshake is complete");
                            SubscriptionHandshakeComplete();
                        break;
                    }
                    case "error":
                    {
                        Debug.Log("The handshake failed. Error: " + message);
                            break;
                    }
                    case "connection_error":
                    {
                        Debug.Log("The handshake failed. Error: " + message);
                            break;
                    }
                    case "data":
                    {
//                        Debug.Log("received data");
                            SubscriptionDataReceived(message);
                        break;
                    }
                    case "ka":
                    {
                        break;
                    }
                    case "subscription_fail":
                    {
                        Debug.Log("The subscription data failed");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }

        private void Init()
        {
            messageQueue.Enqueue("{\"type\":\"connection_init\",\"payload\":{\"Authorization\":\"\"}}");
        }

        private void SendQuery(string id, string query)
        {
            messageQueue.Enqueue(JsonConvert.SerializeObject(new {id = $"{id}", type = "start", payload = new {query = query}}));
        }


        public void SubscriptionHandshakeComplete()
        {
            OnSubscriptionHandshakeComplete?.Invoke();
        }

        public void SubscriptionDataReceived(string data)
        {
            OnSubscriptionDataReceived?.Invoke(data);
        }

        public void SubscriptionCanceled()
        {
            OnSubscriptionCanceled?.Invoke();
        }
    }

	public class HttpHandler
	{
        public static event Action OnRequestBegin;
        public static event Action<Exception> OnRequestFailed;
        public static event Action<string> OnRequestEnded;

        public static async Task<UnityWebRequest> PostAsync(string url, string details, string authToken = null){
            string jsonData = JsonConvert.SerializeObject(new{query = details});
            byte[] postData = Encoding.ASCII.GetBytes(jsonData);
            UnityWebRequest request = UnityWebRequest.Post(url, UnityWebRequest.kHttpVerbPOST);
            request.uploadHandler = new UploadHandlerRaw(postData);
            request.SetRequestHeader("Content-Type", "application/json");
            if (!string.IsNullOrEmpty(authToken)) 
                request.SetRequestHeader("Authorization", "Bearer " + authToken);
            
            OnRequestBegin?.Invoke();
            
            try{
                await request.SendWebRequest();
            }
            catch(Exception e){
                OnRequestFailed?.Invoke(e);
            }
			Debug.Log(request.downloadHandler.text);
            
            OnRequestEnded?.Invoke(request.downloadHandler.text);
            return request;
        }

/*        public static async Task<UnityWebRequest> PostAsync(UnityWebRequest request, string details){
			string jsonData = JsonConvert.SerializeObject(new{query = details});
			byte[] postData = Encoding.ASCII.GetBytes(jsonData);
			request.uploadHandler = new UploadHandlerRaw(postData);
			OnRequestBegin  requestBegin = new OnRequestBegin();
			requestBegin.FireEvent();
            
			try{
				await request.SendWebRequest();
            }
			catch(Exception e){
				Debug.Log("Testing exceptions");
				OnRequestEnded requestFailed = new OnRequestEnded(e);
				requestFailed.FireEvent();
			}
			Debug.Log(request.downloadHandler.text);
            
			OnRequestEnded requestSucceeded = new OnRequestEnded(request.downloadHandler.text);
			requestSucceeded.FireEvent();
			return request;
		}
		
		
        public static async Task<UnityWebRequest> GetAsync(string url, string authToken = null){
            UnityWebRequest request = UnityWebRequest.Get(url);
            if (!string.IsNullOrEmpty(authToken)) 
                request.SetRequestHeader("Authorization", "Bearer " + authToken);
            OnRequestBegin  requestBegin = new OnRequestBegin();
            requestBegin.FireEvent();
            try{
                await request.SendWebRequest();
            }
            catch(Exception e){
                Debug.Log("Testing exceptions");
                OnRequestEnded requestEnded = new OnRequestEnded(e);
                requestEnded.FireEvent();
            }
            Debug.Log(request.downloadHandler.text);
            OnRequestEnded requestSucceeded = new OnRequestEnded(request.downloadHandler.text);
            requestSucceeded.FireEvent();
            return request;
        }*/
        
        #region Websocket

        //Use this to subscribe to a graphql endpoint
		public static async Task<GraphQlSubscriptionWebSocket> WebsocketConnect(string subscriptionUrl, string details, string authToken = null, string socketId = "1", string protocol = "graphql-ws"){
			string subUrl = subscriptionUrl.Replace("http", "ws");
			string id = socketId;
            GraphQlSubscriptionWebSocket cws = new GraphQlSubscriptionWebSocket(subUrl, new List<string>(){protocol}, (!string.IsNullOrEmpty(authToken) ? new Dictionary<string, string>(){{"Authorization", "Bearer " + authToken}} : null));
            try{
				await cws.Connect();
                cws.OnMessage += (data) => OnMessageReceived(cws, data);
                if (cws.State == WebSocketState.Open)
					Debug.Log("connected");
				await WebsocketInit(cws);
				await WebsocketSend(cws, id, details);
			}
			catch (Exception e){
				Debug.Log("woe " + e.Message);
			}

			return cws;
		}
		
		public static async Task<GraphQlSubscriptionWebSocket> WebsocketConnect(GraphQlSubscriptionWebSocket cws, string subscriptionUrl, string details, string socketId = "1"){
			string subUrl = subscriptionUrl.Replace("http", "ws");
			string id = socketId;
            cws = new GraphQlSubscriptionWebSocket(subUrl);
            try
            {
                cws.OnMessage += (data) => OnMessageReceived(cws, data);
				await cws.Connect();
				if (cws.State == WebSocketState.Open)
					Debug.Log("connected");
				await WebsocketInit(cws);
				await WebsocketSend(cws, id, details);
			}
			catch (Exception e){
				Debug.Log("woe " + e.Message);
			}

			return cws;
		}

        private static void OnMessageReceived(GraphQlSubscriptionWebSocket cws, byte[] data)
        {
            string message = Encoding.UTF8.GetString(data ?? throw new ApplicationException("data = null"));

            if (string.IsNullOrEmpty(message))
                return;
            JObject obj;
            try
            {
                obj = JObject.Parse(message);
            }
            catch (JsonReaderException e)
            {
                throw new ApplicationException(e.Message);
            }

            string subType = (string)obj["type"];
            switch (subType)
            {
                case "connection_ack":
                {
                    Debug.Log("init_success, the handshake is complete");
                    cws.SubscriptionHandshakeComplete();
                    break;
                }
                case "error":
                {
                    throw new ApplicationException("The handshake failed. Error: " + message);
                }
                case "connection_error":
                {
                    throw new ApplicationException("The handshake failed. Error: " + message);
                }
                case "data":
                {
                    cws.SubscriptionDataReceived(message);
                    break;
                }
                case "ka":
                {
//                    GetWsReturn(cws);
                    break;
                }
                case "subscription_fail":
                {
                    throw new ApplicationException("The subscription data failed");
                }

            }

		}

        static async Task WebsocketInit(GraphQlSubscriptionWebSocket cws){
            await cws.SendText("{\"type\":\"connection_init\"}");
		}
		
		static async Task WebsocketSend(GraphQlSubscriptionWebSocket cws, string id, string details){
            await cws.SendText(JsonConvert.SerializeObject(new { id = $"{id}", type = "start", payload = new { query = details } }));
		}

        public static async Task WebsocketDisconnect(GraphQlSubscriptionWebSocket cws, string socketId = "1"){ 
			await cws.SendText($"{{\"type\":\"stop\",\"id\":\"{socketId}\"}}");
			await cws.Close();
            cws.SubscriptionCanceled();
        }
		
		#endregion

		#region Utility

		public static string FormatJson(string json)
        {
            var parsedJson = JsonConvert.DeserializeObject(json);
            return JsonConvert.SerializeObject(parsedJson, Formatting.Indented);
        }

		#endregion
	}

    /// <summary>
    /// Class to implement async / await on a UnityWebRequest class
    /// </summary>
    public class UnityWebRequestAwaiter : INotifyCompletion
    {
        private UnityWebRequestAsyncOperation asyncOp;
        private Action continuation;

        public UnityWebRequestAwaiter(UnityWebRequestAsyncOperation asyncOp)
        {
            this.asyncOp = asyncOp;
            asyncOp.completed += OnRequestCompleted;
        }

        public bool IsCompleted { get { return asyncOp.isDone; } }

        public void GetResult() { }

        public void OnCompleted(Action continuation)
        {
            this.continuation = continuation;
        }

        private void OnRequestCompleted(AsyncOperation obj)
        {
            if (continuation != null)
                continuation();
        }
    }

    /// <summary>
    /// Extender to augment UnityWebRequest class
    /// </summary>
    public static class ExtensionMethods
    {
        public static UnityWebRequestAwaiter GetAwaiter(this UnityWebRequestAsyncOperation asyncOp)
        {
            return new UnityWebRequestAwaiter(asyncOp);
        }
    }
}
