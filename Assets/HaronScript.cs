using Mirror.WebRTC;
using System;
using System.Text;
using UnityEngine;

public class HaronScript : MonoBehaviour
{
    public string loginId;
    public RTCTransport rtcTransport;
    public string address;

    private RTCWebSocketSignaler rtcWebSocketSignaler;
    public RTCConnection rtcConnection;

    // Start is called before the first frame update
    void Start()
    {
        InitializeObject();
        ConnectToWebSocketSignaler();

        //StartServer();
        //ConnectClientToServer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void InitializeObject()
    {
        //rtcTransport = GetComponent<RTCTransport>();
        //rtcWebSocketSignaler = (RTCWebSocketSignaler) rtcTransport.activeSignaler;

        rtcWebSocketSignaler = GetComponent<RTCWebSocketSignaler>();
        rtcTransport = GetComponent<RTCTransport>();
        rtcTransport.activeSignaler = rtcWebSocketSignaler;
        rtcTransport.OnClientConnected += SendMessage;
    }

    private void ConnectToWebSocketSignaler()
    {
        Debug.Log($"Attempting to connect as {loginId}");
        rtcWebSocketSignaler.Connect(loginId);

        rtcWebSocketSignaler.WebSocket.OnOpen += ConnectClientToServer;
    }

    private void CloseWebSocketSignaler()
    {
        rtcWebSocketSignaler.Close();
    }

    private void StartServer()
    {
        rtcTransport.ServerStart();
    }

    private void ConnectClientToServer()
    {
        rtcTransport.ClientConnect(address);
        //rtcTransport.clientConnection.DataChannels_OnOpen += SendMessage;
    }

    private void SendMessage()
    {
        Debug.Log("Haron connected to Evans.");
        //Debug.Log("Sending message to server");
        //byte[] msg = Encoding.UTF8.GetBytes("Hello Evans");
        //ArraySegment<byte> msgArray = new ArraySegment<byte>(msg);
        //rtcTransport.ClientSend(rtcTransport.dataChannels.Length - 1, msgArray);
    }

    private void ClientReceiveMessage()
    {
        rtcTransport.OnClientDataReceived += OnClientDataReceived;
    }

    private void OnClientDataReceived(ArraySegment<byte> arg1, int arg2)
    {
        throw new NotImplementedException();
    }

    private void ServerReceiveMessage()
    {
        rtcTransport.OnServerDataReceived += OnServerDataReceived;
    }

    private void OnServerDataReceived(int channelId, ArraySegment<byte> bytes, int arg3)
    {
        var message = Encoding.UTF8.GetString(bytes.Array);
        Debug.Log($"Message from client: {message}");
    }
}