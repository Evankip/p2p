using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using Object = System.Object;


namespace LiquidUniverse
{
    // Object to wrap a string/object into a Unity-Object
    public class WrappepUnityEngineObject : UnityEngine.Object
    {
        public string objectJson;

        public WrappepUnityEngineObject(object obj)
        {
            if (obj != null)
                objectJson = JsonConvert.SerializeObject(obj);
        }

        public WrappepUnityEngineObject(string obj)
        {
            objectJson = obj;
        }
    }
}