namespace LiquidUniverse
{
	using System;
	using System.Collections.Generic;
	using System.Threading.Tasks;
	using Newtonsoft.Json;
	using EosSharp;
	using EosSharp.Unity3D;
	using EosSharp.Core;
	using EosSharp.Core.Providers;
	using EosSharp.Core.Api.v1;
	using ChainAdapter;
	using UnityEngine;
	using LiquidUniverse;

	public partial class WebrtcsignalContract
    {
		public const string contractName = "signal";

		public static async Task<bool> PushSignalTransaction(Signal signal)
		{
			try
			{
				Debug.Log($"<color=green>{await ChainAdapter.eos.CreateTransaction(GenerateSignalTransaction(signal))}</color>");
				return true;
			}
			catch (EosSharp.Core.Exceptions.ApiErrorException ex)
			{
				Debug.LogException(new Exception(ex.ToReadableString("signal")), new WrappepUnityEngineObject(signal));
			}
			catch (EosSharp.Core.Exceptions.ApiException ex)
			{
				Debug.LogException(ex, new WrappepUnityEngineObject(signal));
			}
			catch (Exception ex)
			{
				Debug.LogException(ex, new WrappepUnityEngineObject(signal));
			}
			return false;
		}

		public static Transaction GenerateSignalTransaction(Signal signal)
		{
			return new Transaction()
			{
				actions = new List<EosSharp.Core.Api.v1.Action>()
				{
					new EosSharp.Core.Api.v1.Action()
					{
						account = contractName,
						authorization = new List<PermissionLevel>()
						{
							new PermissionLevel() {actor = ChainAdapter.EosAccountName, permission = "active" }
						},
						name = "signal",
						data = signal
					}
				}
			};
		}

		public static void AddSignalransaction(Signal signal)
		{
			ChainAdapter.unpushedTransactions.Add(GenerateSignalTransaction(signal));
		}
		

		public static async Task<GetTableRowsResponse<SignalMessage>> GetWebrtcsignalTableRows(int limit = 10, string lowerBound = null)
		{
			return await ChainAdapter.eos.GetTableRows<SignalMessage>(new GetTableRowsRequest()
			{
				json = true,
				code = contractName,
				scope = contractName,
				table = "signal",
				lower_bound = lowerBound,
				limit = limit
			});
		}
	}

    public enum WebrtcsignalContractAction
	{
		signal
	}
};
