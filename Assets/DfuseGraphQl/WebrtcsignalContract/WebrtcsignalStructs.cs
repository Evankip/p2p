using Newtonsoft.Json;
using System;

namespace LiquidUniverse
{
    public partial class SignalMessage
    {

        // abi-field-name: type ,abi-field-type: Type
        [JsonProperty(PropertyName = "type")]
        public Type Type;

        // abi-field-name: to ,abi-field-type: name
        [JsonProperty(PropertyName = "to")]
        public string To;

        // abi-field-name: from ,abi-field-type: name
        [JsonProperty(PropertyName = "from")]
        public string From;

        // abi-field-name: data ,abi-field-type: string
        [JsonProperty(PropertyName = "data")]
        public string Data;

        public SignalMessage(Type type, string to, string from, string data)
        {
            this.Type = type;
            this.To = to;
            this.From = from;
            this.Data = data;
        }

        public SignalMessage()
        {
        }
    }
}