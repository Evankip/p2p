using System;
using Newtonsoft.Json;

namespace LiquidUniverse
{
    public partial class Signal 
    {

        // abi-field-name: message ,abi-field-type: signal_message
        [JsonProperty(PropertyName = "message")]
        public SignalMessage Message;

        public Signal(SignalMessage message)
        {
            this.Message = message;
            
        }

        public Signal()
        {
        }
    }

}