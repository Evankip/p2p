using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ChainConfig
{
    /*
     * Standard-Configuration.
     */

    public static string Protocol = "https";

    public static string Host = "8080-apricot-ferret-lkxe4a1r.ws-eu25.gitpod.io";  // just change this to your workspace-url (without Protocol)

    public static int Port = 443;

    public static string ChainId = "df383d1cc33cbb9665538c604daac13706978566e17e5fd5f897eff68b88e1e4";


}
