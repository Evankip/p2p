using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NativeWebSocket;
using System.Threading;
using System.Threading.Tasks;
using DfuseGraphQl;
using GraphQlClient.Core;
using LiquidUniverse;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class DfuseGraphQlClient : MonoBehaviour
{
    public GraphApi DfuseGraphApi;

    private GraphApi.Query query;

    private GraphQlSubscriptionWebSocket _subscriptionWebSocket;

    #region checks

    /// <summary>Starts this instance. Establishes the connection</summary>
    async void Start()
    {
        query = DfuseGraphApi.GetQueryByName("subscription2 ($cursor: String) ", GraphApi.Query.Type.Subscription);
        query.SetArgs("\r\n    query: \"(account:planets OR account:admin)\",\r\n    cursor: $cursor");

        Debug.Log($"<color=yellow>{query.query}</color>");

        /// <summary></summary>
        string graphQlApiUrl = $"{ChainConfig.Protocol}://{ChainConfig.Host}/graphql";

        Debug.Log($"<color=yellow> using {graphQlApiUrl}</color>");

        _subscriptionWebSocket = new GraphQlSubscriptionWebSocket(graphQlApiUrl);

        _subscriptionWebSocket.OnSubscriptionDataReceived += OnSubscriptionDataReceived;
        _subscriptionWebSocket.OnOpen += OnOpen;
        _subscriptionWebSocket.OnError += OnError;
        _subscriptionWebSocket.OnClose += OnClose;

        InvokeRepeating("SendWebSocketMessage", 0.0f, 0.5f);
        await _subscriptionWebSocket.Subscribe(query);
    }

    /// <summary>Regularly called to send messages to connect, reconnect, subscribe</summary>
    void Update()
    {
#if !UNITY_WEBGL || UNITY_EDITOR
        _subscriptionWebSocket.DispatchMessageQueue();
#endif
    }
    private void OnEnable()
    {
        //       if (_subscriptionWebSocket != null)
        //            _subscriptionWebSocket.OnSubscriptionDataReceived += OnSubscriptionDataReceived;
    }

    /// <summary>removes delegate if disabled.</summary>
    private void OnDisable()
    {
        if (_subscriptionWebSocket != null)
            _subscriptionWebSocket.OnSubscriptionDataReceived -= OnSubscriptionDataReceived;
    }

    /// <summary>Closes socket-connection when application is quitting</summary>
    private bool apllicationQuitting = false;
    public async void OnApplicationQuit()
    {
        apllicationQuitting = true;
        if (_subscriptionWebSocket != null)
            await _subscriptionWebSocket.Close();
    }

    /// <summary>Sends the websocket message.</summary>
    async void SendWebSocketMessage()
    {
        if (_subscriptionWebSocket.State == WebSocketState.Open)
        {
            // Sending plain text
            if (_subscriptionWebSocket.messageQueue.Count > 0)
                await _subscriptionWebSocket.SendText(_subscriptionWebSocket.messageQueue.Dequeue());
        }
        else
        {
            Debug.Log($"<color=red>WebSocketState: {_subscriptionWebSocket.State.ToString()}</color>");
        }
    }


    /// <summary>Handles onClose-events.</summary>
    /// <param name="closeCode">The close code.</param>
    private async void OnClose(WebSocketCloseCode closeCode)
    {
        Debug.Log($"<color=red>WebSocketCloseCode: {closeCode.ToString()}</color>");

        if (!apllicationQuitting)
            await _subscriptionWebSocket.Subscribe(query);
    }

    /// <summary>Handles error-messages.</summary>
    /// <param name="errorMsg">The error message.</param>
    private void OnError(string errorMsg)
    {
        Debug.Log($"<color=red>WebsocketErrorMessage: {errorMsg}</color>");
    }

    /// <summary>Handles open-events.</summary>
    private void OnOpen()
    {
        Debug.Log($"<color=green>Websocket opened</color>");
    }

    /// <summary>Called when [subscription data received].</summary>
    /// <param name="subscriptionData">The subscription data.</param>
    public void OnSubscriptionDataReceived(string subscriptionData)
    {
        var dfuseGraphQlResponse = JsonConvert.DeserializeObject<DfuseGraphQlResponse>(subscriptionData);
        switch (dfuseGraphQlResponse.Type)
        {
            case "data":
                ConsumeActions(dfuseGraphQlResponse.Payload.Data.SearchTransactionsForward.Trace.MatchingActions);
                break;
            default:
                Debug.Log($"<color=red>unknown Dfuse-ResponseType</color>");
                break;
        }
    }
    #endregion

    #region ConsumeActions
    /// <summary>Consumes the action-traces received.</summary>
    /// <param name="matchingActions">The matching actions.</param>
    /// <exception cref="System.ArgumentOutOfRangeException"></exception>
    private void ConsumeActions(MatchingAction[] matchingActions)
    {
        try
        {

            foreach (var matchingAction in matchingActions)
            {
                ProcessDeltas(matchingAction.DbOps);

                try
                {
                    switch (matchingAction.Account)
                    {
                        case WebrtcsignalContract.contractName:
                            {
                                WebrtcsignalContractAction action;
                                if (Enum.TryParse(matchingAction.Name, true, out action))
                                {
                                    switch (action)
                                    {
                                        case WebrtcsignalContractAction.signal:
                                            OnWebrtcsignalTrace(matchingAction.Authorization, matchingAction.Json, action);
                                            break;
                                       
                                        default:
                                            Debug.Log(matchingAction.Name + "not found");
                                            break;
                                    }
                                }
                                else
                                {
                                    Debug.Log($"<color=red>{matchingAction.Name} not found</color>");
                                }
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    Debug.Log(matchingAction.Json.ToString());
                    Debug.LogException(ex);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }
    #endregion

    #region WebrtcsignalContract
    /// <summary>Processes the traces.</summary>
    /// <param name="matchingActionAuthorization">The matching action authorization.</param>
    /// <param name="matchingActionJson">The matching action json.</param>
    /// <param name="action">The action.</param>

    private void OnWebrtcsignalTrace(Authorization[] matchingActionAuthorization, object matchingActionJson, WebrtcsignalContractAction action)
    {
        var webrtcsignal = JsonConvert.DeserializeObject<Signalmessage>(matchingActionJson.ToString());
        OnSignal?.Invoke(webrtcsignal);

        Debug.Log(matchingActionJson);
        throw new NotImplementedException();
    }


    #endregion

    #region ProcessDeltas
    /// <summary>Processes the table-deltas.</summary>
    /// <param name="dbOps">The database ops.</param>
    private void ProcessDeltas(DbOp[] dbOps)
    {
        foreach (var matchingActionDbOp in dbOps)
        {
            try
            {
                if (matchingActionDbOp?.Key?.Table == null)
                    continue;

                switch (matchingActionDbOp.Key.Table)
                {
                    
                }
            }
            catch (Exception ex)
            {
                if (matchingActionDbOp?.NewJson?.Object != null)
                    Debug.Log(matchingActionDbOp.NewJson.Object.ToString());
                if (matchingActionDbOp?.OldJson?.Object != null)
                    Debug.Log(matchingActionDbOp.OldJson.Object.ToString());
                Debug.LogException(ex);
            }
        }
#if DEBUG
        if (dbOps?.Length > 0)
            Debug.Log(JsonConvert.SerializeObject(dbOps));
#endif
    }
    #endregion

    #region Events

    public event Action<Signalmessage> OnSignal;

    #endregion

}

