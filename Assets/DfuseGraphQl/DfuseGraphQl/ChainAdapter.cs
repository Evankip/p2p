
using System.Runtime.CompilerServices;
using System.Threading.Tasks;   // Do not delete
using UnityEngine;

namespace ChainAdapter
{
    using EosSharp.Unity3D;
    using Newtonsoft.Json;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using UnityEngine;
    using EosSharp.Core.Api.v1;
    using EosSharp.Core.Providers;
    using EosSharp.Core.Exceptions;
    using Transaction = EosSharp.Core.Api.v1.Transaction;


    public class ChainAdapter
    {
        public static bool isDefault = false;
        public static bool connected = false;

        public static string privateKey = "";
        public static string EosAccountName = "";

        private static bool isWaitingForSignature = false;

        private static bool IsWaitingForSignature
        {
            get { return isWaitingForSignature; }
            set
            {
                isWaitingForSignature = value;
                //UniverseGameManager.instance.TxSigningOverlay?.ShowWaitingForSignature(value);
            }
        }

        internal static async Task<object> CreateTransaction(Transaction transaction)
        {
            if (IsWaitingForSignature)
                throw new Exception("There's already transaction in progress");
            try
            {
                IsWaitingForSignature = true;
                return await eos.CreateTransaction(transaction);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                IsWaitingForSignature = false;
            }
        }

        public static Eos eos = new Eos(new EosSharp.Core.EosConfigurator()
        {
            //ChainId = network.chainId,
            //ttpEndpoint = network.GetHttpEndpoint()
        });


        public static void SetSignatureProvider(string privateKey, string accountName)
        {
            ChainAdapter.EosAccountName = accountName;
            ChainAdapter.privateKey = privateKey;
            eos = new Eos(new EosSharp.Core.EosConfigurator()
            {
                //ChainId = network.chainId,
                //HttpEndpoint = network.GetHttpEndpoint(),
                //SignProvider = new DefaultSignProvider(privateKey)
            });
            isDefault = true;
        }

        //public static void SetIdentity(Identity identity, MonoBehaviour scriptInstance)
        //{
        //    scatter = new Scatter(new ScatterConfigurator()
        //    {
        //        AppName = "LIQUIDUNIVERSEDEMO",
        //        Network = network,
        //        StorageProvider = fileStorage
        //    }, scriptInstance);

        //    scatter.Identity = identity;
        //    isScatter = true;
        //    isDefault = !isScatter;
        //}

        
        public static async Task<object> PushUnpushedTransactions()
        {
            Transaction transaction = new Transaction();
            transaction.actions = new List<EosSharp.Core.Api.v1.Action>();

            foreach (var unpushedTransaction in unpushedTransactions)
            {
                transaction.actions.AddRange(unpushedTransaction.actions);
            }

            return await CreateTransaction(transaction);
        }

        public static List<Transaction> unpushedTransactions = new List<Transaction>();
    }
}