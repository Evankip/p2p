using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using LiquidUniverse;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Debug = UnityEngine.Debug;

public static class Cache 
{
    public static Dictionary<Type, SignalMessage> Messsage = new Dictionary<Type, SignalMessage>();


    public static async Task<bool> Update()
    {
       
        var signalmessageTask = GetFullSignalMessageTableState(2);

        await Task.WhenAll(signalmessageTask);

        Messsage = signalmessageTask.Result;

        return true;
    }

    private static async Task<Dictionary<Type, SignalMessage>> GetFullSignalMessageTableState(int progressPercentage)
    {
        try
        {
            var getWebrtcsigTableRowsResponse = await WebrtcsignalContract.GetWebrtcsignalTableRows(int.MaxValue);
            var messageList = getWebrtcsigTableRowsResponse.rows;
            while (getWebrtcsigTableRowsResponse.more)
            {
                getWebrtcsigTableRowsResponse = await WebrtcsignalContract.GetWebrtcsignalTableRows(int.MaxValue, getWebrtcsigTableRowsResponse.next_key);
                messageList.AddRange(getWebrtcsigTableRowsResponse.rows);
            }

            return messageList.GroupBy(p => p.Type).ToDictionary(p => p.Key, p => p.First());
        }
        catch (Exception e)
        {

            return new Dictionary<Type, SignalMessage>();
        }
    }


}
