using System.Collections;
using System.Collections.Generic;
using Mirror.WebRTC;
using System;
using LiquidUniverse;
using UnityEngine;
using UnityEngine.UIElements;

public class ChatBox :MonoBehaviour
{
    /*
    * Connected Views
    */
    

    /*
     * Child-Controls
     */

    private TextField _messageText;
    private Button _sendButton;
    public VisualElement _sentMessageBox;
    public VisualElement _receivedMessageBox;

    /*
     * Fields, Properties
     */
    public SignalMessage _signalMessage;
    

    public string messageInput;


    void Start()
    {
        //Grab a reference to the root element that is drawn
        var Root = GetComponent<UIDocument>().rootVisualElement;

        // _messageText = Root.Q<TextField>("message-text-field");
         _sendButton = Root.Q<Button>("send-button");
        // _sentMessageBox = Root.Q<Button>("sent-message-box");
        // _receivedMessageBox = Root.Q<Button>("received-message-box");

        //Root.style.visibility = Visibility.Hidden;

        
        //from = ChainAdapter.ChainAdapter.EosAccountName;



        //_messageText.value = messageInput;
        BindDfuseEvents();
        BindButtons();
    }

    private void BindButtons()
    {
        _sendButton.clickable.clicked += async () =>
        {
            await WebrtcsignalContract.PushSignalTransaction(new Signal(_signalMessage));
        };
    }
    public Type Type1;
    private void BindDfuseEvents()
    {

        // Attach event-handlers when View is shown
        //OnShow += () =>
        //{

        //};

        // Remove event-handlers when View is hidden
        //OnHide += () =>
        //{

        //};

        // Create the SinganlMessage-Object that is later passed to the blockchain
        _signalMessage = new SignalMessage(Type1, "haron", ChainAdapter.ChainAdapter.EosAccountName, "data" );
        
        
    }


}
